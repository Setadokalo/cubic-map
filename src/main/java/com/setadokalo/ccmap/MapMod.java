package com.setadokalo.ccmap;

import net.minecraft.init.Blocks;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import org.apache.logging.log4j.Logger;

@Mod(modid = MapMod.MODID, name = MapMod.NAME, version = MapMod.VERSION)
public class MapMod
{
	public static final String MODID = "ccmap";
	public static final String NAME = "Cubic Chunks Mapper";
	public static final String VERSION = "0.0.0";

	protected static Logger logger;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event)
	{
		logger = event.getModLog();
	}

	@EventHandler
	public void init(FMLInitializationEvent event)
	{
		// some example code
	}
}
