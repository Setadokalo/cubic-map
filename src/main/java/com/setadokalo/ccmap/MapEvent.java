package com.setadokalo.ccmap;

import cubicchunks.api.CubeWatchEvent;
import cubicchunks.api.worldgen.populator.CubePopulatorEvent;
import cubicchunks.world.ICubicWorldClient;
import cubicchunks.world.column.IColumn;
import cubicchunks.world.cube.Cube;
import net.minecraft.block.BlockClay;
import net.minecraft.block.BlockNetherrack;
import net.minecraft.block.BlockSand;
import net.minecraft.client.multiplayer.WorldClient;
import net.minecraft.util.math.BlockPos;
import net.minecraft.world.IWorldEventListener;
import net.minecraft.world.World;
import net.minecraft.world.chunk.Chunk;
import net.minecraftforge.event.world.ChunkEvent;
import net.minecraftforge.event.world.WorldEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.relauncher.Side;

@Mod.EventBusSubscriber(Side.CLIENT)
public class MapEvent {
	@SubscribeEvent
	public static void worldload(WorldEvent.Load event) {
		event.getWorld().addEventListener(new MapWorldEventListener());
	}

}